package com.ddimitrovd.videoplayerlib

import android.Manifest
import android.os.Environment
import android.support.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith
import android.support.test.rule.GrantPermissionRule
import org.junit.Assert.assertTrue
import org.junit.Rule


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class PlayerLoadInstrumentedTest {

    @Rule @JvmField
    var mRuntimePermissionRuleWrite = GrantPermissionRule.grant(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    @Rule @JvmField
    var mRuntimePermissionRuleRead = GrantPermissionRule.grant(Manifest.permission.READ_EXTERNAL_STORAGE)
    @Rule @JvmField
    var mRuntimePermissionRuleInternet = GrantPermissionRule.grant(Manifest.permission.INTERNET)

    // !!! Unfortunately Granting permissions doesn't work

    @Test
    fun useAppContext() {
        val path = Environment.getExternalStorageDirectory().toString()
        val player = Player()
        player.load(path,"https://bitmovin-a.akamaihd.net/content/MI201109210084_1/mpds/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.mpd")
        Thread.sleep(1000)
        assertTrue(player.isReady())
    }
}