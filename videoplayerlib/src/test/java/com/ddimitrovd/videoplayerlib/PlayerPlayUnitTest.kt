package com.ddimitrovd.videoplayerlib

import com.ddimitrovd.eventbuslib.EventBus
import com.ddimitrovd.videoplayerlib.event.common.PlayerEventType
import com.ddimitrovd.videoplayerlib.fake.FakeEventListener
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

class PlayerPlayUnitTest {

    @Rule
    @JvmField
    val folder =  TemporaryFolder()

    var player = Player()

    @Before
    fun load() {
        // create temp file
        val tempFile = folder.newFile("temp.mpd")

        // create a player
        player = Player()

        // load from file
        player.load(tempFile)
    }

    @Test
    fun isPlayingNegativeTest() {
        assertFalse(player.isPlaying())
    }

    @Test
    fun isPlayingPositiveTest() {
        player.play()
        assertTrue(player.isPlaying())
    }

    @Test
    fun isPlayingEventTest() {
        // subscribe to event
        val eventListener = FakeEventListener()
        EventBus.subscribeToEvent(PlayerEventType.ON_PLAY, eventListener)

        // call the function
        player.play()
        Thread.sleep(100)

        // Expect lay event on listener
        assertTrue(eventListener.events[0].getEventType() == PlayerEventType.ON_PLAY)
    }
}