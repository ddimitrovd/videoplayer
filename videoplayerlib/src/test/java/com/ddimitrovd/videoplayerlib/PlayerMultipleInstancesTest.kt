package com.ddimitrovd.videoplayerlib

import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

class PlayerMultipleInstancesTest {

    @Rule
    @JvmField
    val folder =  TemporaryFolder()

    var playerA = Player()
    var playerB = Player()

    @Before
    fun load() {
        // create temp file
        val tempFile = folder.newFile("temp.mpd")

        // create players
        playerA = Player()
        playerB = Player()

        // load from file
        playerA.load(tempFile)
        playerB.load(tempFile)
    }

    @Test
    fun multiplePlayersPlaytimeTest() {
        // play on A
        playerA.play()

        // pause
        Thread.sleep(300)
        playerA.pause()
        val timeA = playerA.getCurrentTime()

        // B time should be unchanged
        assertTrue(playerB.getCurrentTime() == 0 && timeA > 0)

        // play on B
        playerB.play()

        // pause
        Thread.sleep(300)
        playerB.pause()

        // A time should be unchanged
        assertTrue(playerB.getCurrentTime() > 0 && timeA == playerA.getCurrentTime())

    }
}