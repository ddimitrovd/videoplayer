package com.ddimitrovd.videoplayerlib

import com.ddimitrovd.eventbuslib.EventBus
import com.ddimitrovd.videoplayerlib.event.common.ErrorEvent
import com.ddimitrovd.videoplayerlib.event.common.PlayerEventType
import com.ddimitrovd.videoplayerlib.fake.FakeEventListener
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import java.net.MalformedURLException

class PlayerLoadUnitTest {

    @Rule @JvmField
    val folder =  TemporaryFolder()

    @Test
    fun loadingFromNonURLFormatTest(){
        // create a player
        val player = Player()

        // subscribe to event
        val eventListener = FakeEventListener()
        EventBus.subscribeToEvent(PlayerEventType.ON_ERROR, eventListener)

        // call the function
        player.load("path/","thisIsNotAValidURL")
        Thread.sleep(100)

        // Expect URI exception
        assertTrue(eventListener.events[0] is ErrorEvent && (eventListener.events[0] as ErrorEvent).exception is MalformedURLException)
    }


    @Test
    fun loadingStatusTest(){
        // create a player
        val player = Player()

        // should not be loaded
        assertFalse(player.isLoaded())
    }


    @Test
    fun loadingFromLocalFileTest(){
        // create temp file
        val tempFile = folder.newFile("temp.mpd")

        // create a player
        val player = Player()

        // load from file
        player.load(tempFile)

        // should be loaded
        assertTrue(player.isLoaded())
    }


    @Test
    fun loadingEventTest(){
        // subscribe to event
        val eventListener = FakeEventListener()
        EventBus.subscribeToEvent(PlayerEventType.ON_LOADED, eventListener)

        // create temp file
        val tempFile = folder.newFile("temp.mpd")

        // create a player
        val player = Player()

        // load from file
        player.load(tempFile)

        // wait for video to finish
        Thread.sleep(1000)

        // check events in listener
        assertTrue(eventListener.events[0].getEventType() == PlayerEventType.ON_LOADED)
    }
}