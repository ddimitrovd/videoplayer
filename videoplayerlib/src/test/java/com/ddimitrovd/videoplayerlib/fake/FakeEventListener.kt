package com.ddimitrovd.videoplayerlib.fake

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.Listener

class FakeEventListener:Listener {

    val events = arrayListOf<Event>()

    override fun onEventReceived(event: Event) {
        events.add(event)
    }
}