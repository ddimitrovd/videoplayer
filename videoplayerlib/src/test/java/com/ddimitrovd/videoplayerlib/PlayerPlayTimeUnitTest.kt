package com.ddimitrovd.videoplayerlib

import com.ddimitrovd.eventbuslib.EventBus
import com.ddimitrovd.videoplayerlib.event.common.PlayerEventType
import com.ddimitrovd.videoplayerlib.event.common.StatusEvent
import com.ddimitrovd.videoplayerlib.fake.FakeEventListener
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

class PlayerPlayTimeUnitTest {

    @Rule
    @JvmField
    val folder =  TemporaryFolder()

    var player = Player()

    @Before
    fun load() {
        // create temp file
        val tempFile = folder.newFile("temp.mpd")

        // create a player
        player = Player()

        // load from file
        player.load(tempFile)
    }

    @Test
    fun currentTimeTest() {
        // currentTime before playing is zero
        assertEquals(0, player.getCurrentTime())

        // play
        player.play()

        // wait
        Thread.sleep(200)

        // expect time to have advanced
        assertTrue(player.getCurrentTime() > 0)
    }

    @Test
    fun videoResumesCorrectlyTest() {
        // start playing video
        player.play()

        // wait
        Thread.sleep(200)

        // pause and take time
        player.pause()
        val time = player.getCurrentTime()

        // resume
        player.play()

        // check that is has resumed from paused instance of time
        assertTrue(time != 0 && time <= player.getCurrentTime())
    }


    @Test
    fun playbackFinishedEventTest() {
        // subscribe to event
        val eventListener = FakeEventListener()
        EventBus.subscribeToEvent(PlayerEventType.ON_PLAYBACKFINISHED, eventListener)

        // play video
        player.play()

        // wait for video to finish
        Thread.sleep(1000)

        // check events in listener
        assertTrue(eventListener.events[0].getEventType() == PlayerEventType.ON_PLAYBACKFINISHED)
        assertFalse(player.isPlaying())
    }
}