package com.ddimitrovd.videoplayerlib

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventBus
import com.ddimitrovd.eventbuslib.Listener
import com.ddimitrovd.eventbuslib.SubscriptionPriority
import com.ddimitrovd.videoplayerlib.event.common.ErrorEvent
import com.ddimitrovd.videoplayerlib.event.common.PlayerEventType
import com.ddimitrovd.videoplayerlib.event.common.StatusEvent
import com.ddimitrovd.videoplayerlib.event.internal.DownloadedEvent
import com.ddimitrovd.videoplayerlib.event.internal.InternalEventType
import java.io.File
import java.net.URL
import java.util.concurrent.Executors
import java.util.concurrent.Future


/**
 * The player class can load a stream manifest and provides the standard calls for a media player such as play and pause.
 */
//TODO: The player needs dynamic permissions for read and write on external storage.
// I am note sure If the Player lib should request them or the client app.
class Player: Listener {

    private var loadedManifestFile: File? = null

    private var videoLoader: VideoLoader? = null
    private var videoLoaderFuture: Future<*>? = null
    private var isLoading = false
    private var lastPausedTime = 0

    private var playerService: PlayerService? = null
    private var playerServiceFuture: Future<*>? = null
    private var isPlaying = false


    override fun onEventReceived(event: Event) {
        when{
            event.getEventType() == InternalEventType.MANIFEST_DOWNLOADED_EVENT -> onLoaded(event)
            event.getEventType() == PlayerEventType.ON_PLAYBACKFINISHED -> onPlaybackFinished(event)
            event.getEventType() == InternalEventType.DOWNLOADING_ERROR_EVENT -> publishError(java.lang.Exception()) //A good idea here would be to try several more times to download file (not implemented)
        }
    }


    //TODO: addListener and removeListener
    // The player should provide those by warping the EventBus (not implemented)
    // I would go by implementing this by simply linking the subscribers to the EventBus. That is why
    // I added the player instance in the data carried in the Events. So that Multiple players can be supported
    // Player should not know of the subscribers as it becomes more like observer pattern


    //region ----- LOAD/UNLOAD -----------------------------------------------------------------------------------------


    /**
     * Load a video to the player from an URL
     *
     * @param savePath the path to local storage where the manifest file will be temporary stored
     * @param urlString the URL form string representing the location of the video manifest
     */
    fun load(savePath: String, urlString: String){
        // TODO: check if savePath directory exists and we have permission to read/write (not implemented)

        // unload previous video file and reset values
        unload()

        // create file path
        val path = savePath + urlString.substring(urlString.lastIndexOf("/")+1)

        // subscribe to internal loading events
        EventBus.subscribeToEvent(InternalEventType.MANIFEST_DOWNLOADED_EVENT,this,SubscriptionPriority.HIGH)
        EventBus.subscribeToEvent(InternalEventType.DOWNLOADING_ERROR_EVENT,this,SubscriptionPriority.HIGH)
        isLoading = true

        // register executor for the play service and execute
        try {
            videoLoader = VideoLoader(URL(urlString), path)
            val executorService = Executors.newSingleThreadExecutor()
            videoLoaderFuture = executorService.submit(videoLoader)
            EventBus.acceptEvent(StatusEvent(PlayerEventType.ON_LOADED, this))
        } catch (e: Exception) {
            publishError(e)
        }
    }


    /**
     * Load a video to the player from a manifest File
     *
     * @param manifestFile A local video manifest file for the video to be loaded
     */
    fun load(manifestFile: File){
        // unload previous video file and reset values
        unload()

        // check passed file
        if (manifestFile.exists()) { // TODO: Probably also check if file is what we want (not implemented)
            loadedManifestFile = manifestFile
            afterSuccessfulLoading()
            EventBus.acceptEvent(StatusEvent(PlayerEventType.ON_LOADED, this))
        } else {
            publishError(java.lang.Exception())
        }
    }


    /**
     * Unload the loaded video and delete all related files
     */
    fun unload(){
        // pause in case something is playing, this also stops player service if any
        pause()

        // stop videoLoader service if running
        if (videoLoaderFuture != null)
            videoLoaderFuture!!.cancel(true)

        // unsubscribe from any loading internal events
        unsubscribeFromInternalLoadingEvents()

        // Delete loaded file if any
        // TODO: If the file was passed by the user maybe we shouldn't delete it as he wants to keep it?
        if (loadedManifestFile != null)
            loadedManifestFile!!.delete()

        // clear flags
        lastPausedTime = 0
        isLoading = false
        isPlaying = false
    }


    private fun onLoaded(event: Event) {
        // check event type
        if (event is DownloadedEvent){
            // check if it is the file that this player instance asked for
            if(event.videoLoader == videoLoader) {
                isLoading = false
                loadedManifestFile = File(event.filePath)
                // check that file was indeed created
                if (loadedManifestFile!!.exists()) {
                    // successful loading
                    afterSuccessfulLoading()
                } else {
                    // file if not created... maybe try to download again (not implemented)
                    publishError(java.lang.Exception())
                }
            }
        } else {
            // Event is not of corresponding class
            publishError(java.lang.Exception())
        }
    }


    /**
     * Is true after the loading process has a valid video manifest file
     */
    fun isLoaded(): Boolean {
        return loadedManifestFile != null && loadedManifestFile!!.exists()
    }


    private fun afterSuccessfulLoading() {
        unsubscribeFromInternalLoadingEvents()
        EventBus.acceptEvent(StatusEvent(PlayerEventType.ON_READY, this))
        lastPausedTime = 0
    }


    private fun unsubscribeFromInternalLoadingEvents() {
        EventBus.unsubscribeFormEvent(InternalEventType.MANIFEST_DOWNLOADED_EVENT,this)
        EventBus.unsubscribeFormEvent(InternalEventType.DOWNLOADING_ERROR_EVENT,this)
    }


    //endregion


    //region ----- PLAYBACK --------------------------------------------------------------------------------------------


    /**
     * Play the loaded video if there is a loaded video and player is ready
     */
    fun play(){
        if (!isPlaying && isReady()){
            isPlaying = true
            playerService = PlayerService(loadedManifestFile!!, lastPausedTime, this)

            // register executor for the play service and execute
            val executorService = Executors.newSingleThreadExecutor()
            playerServiceFuture = executorService.submit(playerService)

            // subscribe to playback event
            EventBus.subscribeToEvent(PlayerEventType.ON_PLAYBACKFINISHED,this,SubscriptionPriority.HIGH)

            // Submit event
            EventBus.acceptEvent(StatusEvent(PlayerEventType.ON_PLAY, this))
        }
    }


    /**
     * Pause the video if it is playing
     */
    fun pause() {
        if (isPlaying && playerService!= null && playerServiceFuture != null){
            isPlaying = false
            lastPausedTime = playerService!!.currentTime

            // cancel playback thread
            playerServiceFuture!!.cancel(true)

            // unsubscribe from playback event
            EventBus.unsubscribeFormEvent(PlayerEventType.ON_PLAYBACKFINISHED,this)

            // Submit event
            EventBus.acceptEvent(StatusEvent(PlayerEventType.ON_PAUSE, this))
        }
    }


    /**
     * Player is ready to play the loaded video
     */
    fun isReady(): Boolean {
        return isLoaded() // In the case of this limited implementation those methods are practically the same
    }


    /**
     * Player is currency playing the video
     */
    fun isPlaying(): Boolean {
        return isPlaying
    }


    /**
     * Video is currently paused or never started playing
     */
    fun isPaused(): Boolean {
        return isReady() && !isPlaying()
    }


    private fun onPlaybackFinished(event: Event) {
        // check event type and if it comes this instance's playerService
        if (event is StatusEvent && event.player == this){
            isPlaying = false
            lastPausedTime = 0

            // unsubscribe from playback event
            EventBus.unsubscribeFormEvent(PlayerEventType.ON_PLAYBACKFINISHED,this)
        }
    }


    //endregion

    /**
     * Returns the duration of the loaded video if any
     *
     * @return Duration of video in seconds or 0 if no video loaded
     */
    fun getDurration(): Int{
        if (isReady())
             return getVideoDurationSecondsFromManifest(loadedManifestFile!!)
        return 0
    }


    /**
     * Returns the current time in the loaded video if any
     *
     * @return Current time in video in seconds or 0 if no video loaded
     */
    fun getCurrentTime(): Int{
        if (isReady()){
            return if (isPlaying && playerService != null)
                playerService!!.currentTime
            else
                lastPausedTime
        }
        return 0
    }


    /**
     * Destroy all files related to the player
     */
    fun destroy() {
        return unload() // In the case of this limited implementation those methods are practically the same ans unload clears all data
        // TODO: It is important to mention that if addListener and RemoveListener were implemented for the Player
        //  it would have to unsubscribe them as well on top of what unload() does.
    }


    private fun publishError(e: Exception) {
        // TODO: All mare just just representative. In a complete implementation I would define custom Exceptions
        //  for more descriptive situations
        EventBus.acceptEvent(ErrorEvent(PlayerEventType.ON_ERROR,e))
    }

}