package com.ddimitrovd.videoplayerlib

import com.ddimitrovd.eventbuslib.EventBus
import com.ddimitrovd.videoplayerlib.event.common.PlayerEventType
import com.ddimitrovd.videoplayerlib.event.common.StatusEvent
import java.io.File


/**
 * Player Service simulates a the video being played at x10 speed by
 * running a loop that sleeps fo 100ms and increments video time
 */
internal class PlayerService(val streamManifest: File, val startTime: Int, val owner: Player): Runnable {

    var currentTime = startTime
        private set

    private val videoDuration = getVideoDurationSecondsFromManifest(streamManifest)

    override fun run() {
        // play loop
        while (currentTime < videoDuration){
            currentTime++
            Thread.sleep(100) // simulating fast playback
        }


        // playback finished
        if (videoDuration <= currentTime){
            EventBus.acceptEvent(StatusEvent(PlayerEventType.ON_PLAYBACKFINISHED, owner))
        }

    }
}