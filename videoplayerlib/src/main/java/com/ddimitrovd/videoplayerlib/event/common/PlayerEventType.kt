package com.ddimitrovd.videoplayerlib.event.common

import com.ddimitrovd.eventbuslib.EventType

enum class PlayerEventType: EventType {
    ON_READY,
    ON_TIME_CHANGED,
    ON_LOADED,
    ON_UNLOADED,
    ON_PLAY,
    ON_PAUSE,
    ON_PLAYBACKFINISHED,
    ON_ERROR;
}