package com.ddimitrovd.videoplayerlib.event.internal

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventType
import com.ddimitrovd.videoplayerlib.VideoLoader

internal class DownloadedEvent(private val eventType: EventType, val filePath: String, val videoLoader: VideoLoader): Event {

    override fun getEventType(): EventType {
        return eventType
    }
}