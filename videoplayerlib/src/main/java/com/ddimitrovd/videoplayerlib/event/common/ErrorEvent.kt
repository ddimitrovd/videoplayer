package com.ddimitrovd.videoplayerlib.event.common

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventType
import java.lang.Exception

class ErrorEvent(private val eventType : EventType, val exception: Exception): Event {
    override fun getEventType(): EventType {
        return eventType
    }
}