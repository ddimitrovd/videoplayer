package com.ddimitrovd.videoplayerlib.event.common

import com.ddimitrovd.eventbuslib.Event
import com.ddimitrovd.eventbuslib.EventType
import com.ddimitrovd.videoplayerlib.Player

class StatusEvent(private val eventType: EventType, val player: Player): Event {

    override fun getEventType(): EventType {
        return eventType
    }
}