package com.ddimitrovd.videoplayerlib.event.internal

import com.ddimitrovd.eventbuslib.EventType

internal enum class InternalEventType: EventType {
    MANIFEST_DOWNLOADED_EVENT,
    DOWNLOADING_ERROR_EVENT;
}