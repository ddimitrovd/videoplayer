package com.ddimitrovd.videoplayerlib

import com.ddimitrovd.eventbuslib.EventBus
import com.ddimitrovd.videoplayerlib.event.internal.InternalEventType
import com.ddimitrovd.videoplayerlib.event.internal.DownloadErrorEvent
import com.ddimitrovd.videoplayerlib.event.internal.DownloadedEvent
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import java.net.URL

/**
 * VideoLoader is a small service that loads the stream manifest file. Firing an event depending of the outcome
 * It does not Implement Download Manager as it is a very short task that does not to be known tho the end user.
 */
internal class VideoLoader(val url: URL, val filePath: String): Runnable {

    override fun run() {
        try {
            // get the file and save to external storage
            url.openStream().use { input ->
                FileOutputStream(File(filePath)).use { output ->
                    input.copyTo(output)
                }
            }
            EventBus.acceptEvent(DownloadedEvent(InternalEventType.MANIFEST_DOWNLOADED_EVENT,filePath, this))
        } catch (e : Exception) {
            EventBus.acceptEvent(DownloadErrorEvent(InternalEventType.DOWNLOADING_ERROR_EVENT, filePath, e, this))
        }
    }
}