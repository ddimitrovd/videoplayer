package com.ddimitrovd.videoplayerlib

import java.io.File

const val HARDCODED_VIDEO_DURATION = 8

internal fun getVideoDurationSecondsFromManifest(manifest: File): Int {
    return HARDCODED_VIDEO_DURATION
}